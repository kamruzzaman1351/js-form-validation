// Select all the Form Elements
const form = document.getElementById("form");
const username = document.getElementById("username");
const email = document.getElementById("email");
const password = document.getElementById("password");
const password2 = document.getElementById("password2");

// Error Massage
function showErrorMassage(input, msg) {
    const formControl = input.parentElement;
    formControl.className = "form-control error";
    const small = formControl.querySelector("small");
    small.innerText = msg;
}

// Success Massage 
function showSuccessMassage(input) {
    const formControl = input.parentElement;
    formControl.className = "form-control success";
}

// Valid Email Check
function emailValidCheck(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLocaleLowerCase());
}

// Check valid email
function validEmailCheck(input) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(input.value.trim()).toLocaleLowerCase())) {
        showErrorMassage(input, "Email is not valid")
    }
}

// Check all inputs one by one
function inputRequireCheck(inputArr) {
    inputArr.forEach(input => {
        if(input.value.trim() === ''){
            showErrorMassage(input, `${getFieldName(input)} is required`);
        } else {
            showSuccessMassage(input);
        } 
    });
}

// Get Field Name
function getFieldName(input) {
    return input.id.charAt(0).toUpperCase() + input.id.slice(1) ;
}

// Input field length Check
function checkInputLength(input, min, max) {
    if(input.value.length < min) {
        showErrorMassage(input, `${getFieldName(input)} must be at least ${min} character`);        
    } else if(input.value.length > max) {
        showErrorMassage(input, `${getFieldName(input)} must be less than ${max} character`);
    } else{
        showSuccessMassage(input);
    }
}

// Check Password Match

function checkPasswordMatch(input, input1) {
    if(input.value !== input1.value) {
        showErrorMassage(input1, `Password do not match`)
    }
}
// Form Submit function
function formSubmit(e) {
    e.preventDefault(); 
    if(username.value === "") {
        showErrorMassage(username, "Username is required");
    } else{
        showSuccessMassage(username);
    }
    if(email.value === "") {
        showErrorMassage(email, "Email is required");
    } else if(!emailValidCheck(email.value)) {
        showErrorMassage(email, "Email is not valid");
    }else{
        showSuccessMassage(email);
    }
    if(password.value === "") {
        showErrorMassage(password, "Password is required");
    } else{
        showSuccessMassage(password);
    }
    if(password2.value === "") {
        showErrorMassage(password2, "Confirm Password is required");
    } else{
        showSuccessMassage(password2);
    }
}

// Form submit second function (Best approach)
function formSubmitSecond(e) {
    e.preventDefault();
    inputRequireCheck([username,email,password,password2]);
    checkInputLength(username, 3, 15);
    checkInputLength(password, 6, 22);
    checkPasswordMatch(password, password2);
    validEmailCheck(email);
}


// Form Submit 
form.addEventListener("submit", formSubmitSecond);